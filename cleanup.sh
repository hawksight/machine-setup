#!/bin/bash
# May require sudo
set -e

repo_root=$(pwd)
config_file="config.yml"
venv=".python"

echo "Cleaning up dependencies ..."
# Disable this for now whilst testing
# echo "Removing config file..."
# rm "${repo_root}/config.yml"
echo "Removing ansible-galaxy deps..."
rm -rf "${repo_root}/roles"
echo "Removing python virtual env..."
rm -rf "${repo_root}/${venv}"

printf "Script Completed: All setup removed\n\n"
exit 0
