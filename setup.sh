#!/bin/bash
# May require sudo
set -e

echo "Setting up dependencies..."

# Set some global variables
current_dir=$(pwd)
repo_root=$(pwd)
config_file="config.yml"
ansible_playbook="macos.yaml"
ansible_galaxy_deps="ansible-galaxy-reqs.yml"
venv=".python"
venv_python="${current_dir}/${venv}/bin/python"
venv_pip="${current_dir}/${venv}/bin/pip"
venv_ansible="${current_dir}/${venv}/bin/ansible"
venv_ansible_galaxy="${current_dir}/${venv}/bin/ansible-galaxy"
venv_ansible_playbook="${current_dir}/${venv}/bin/ansible-playbook"


# Make python virtual environments
if [ -d "${venv}" ] ; then
    echo "Virtual environment exists in ${current_dir} skipping python setup"
else
    echo "No python3 virtual environment in ${current_dir}, setting up"
    python3 -m venv ${venv}
    ${venv_pip} install -U pip
    ${venv_pip} install -U ansible
    echo "Virtual environment setup in ${current_dir}, continuing"
fi
echo ""

# Check for ansible-galaxy reqs
if [ -d "${current_dir}/roles" ] ; then
    echo "Roles direcotry exists: ${current_dir}/roles, reinstalling..."
    ${venv_ansible_galaxy} role install -r ${current_dir}/${ansible_galaxy_deps}
    echo "Roles installed succesfully"
else
    echo "Setting up python virtual environment ..."
    ${venv_ansible_galaxy} role install -r ${current_dir}/${ansible_galaxy_deps}
    echo "Ansible requirements installed, continuing"
fi
echo ""

# Now it's ready to run so output command
# TODO: Make this run automagically.
echo "Setup Command..."
printf "${venv_ansible_playbook} ${ansible_playbook} -i inventory --ask-become-pass\n"
printf "\nScript Completed\n"
exit 0

