#!/bin/bash
# Requires root

set -e

# Left this here or reference
# https://stackoverflow.com/a/3588939
#echo
#echo "# arguments called with ---->  ${@}     "
#echo "# \$1 ---------------------->  $1       "
#echo "# \$2 ---------------------->  $2       "
#echo "# path to me --------------->  ${0}     "
#echo "# parent path -------------->  ${0%/*}  "
#echo "# my name ------------------>  ${0##*/} "
#echo

# https://askubuntu.com/a/15856
# env vars with sudo: https://www.petefreitag.com/item/877.cfm
# Check running as root / sudo
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run with root privileges eg."
   echo "Usage: sudo ./${0}" 
   echo
   echo "Or pass in user with an env var, eg."
   echo "Usage: sudo MS_USER=example ./${0}"
   exit 1
fi

if [[ -z "${MS_USER}" ]] ; then
    echo "MS_USER env var is empty, please input username to give sudo to.."
    read -p 'Username: ' user
    echo "Thankyou, ${user} will be given sudo access"
else
    user=${MS_USER}
    echo "MS_USER set to: ${user}"
    echo "This user will be given sudo access"
fi

# TODO: Support linux
arch=$(uname)
case ${arch} in
    "Darwin") 
        echo "Running on MacOS - ${arch} - SUPPORTED"
        ;;
    *)
        echo "Running on unknown - ${arch} - NOT SUPPORTED"
        echo "Cannot continue, exiting .."
        exit 1
        ;;
esac

# TODO: Move this into a function and run only if on MacOS
# https://apple.stackexchange.com/a/325089
os=$(sw_vers -productVersion | awk -F. '{print $1 "." $2}')
echo "Running on macOS: ${os}"
echo "Checking for osx Command Line Tools ..."
if softwareupdate --history | grep --silent 'Command Line Tools' ; then
    echo 'Command-line tools already installed.' 
else
    echo 'Installing Command-line tools ...'
    # https://gist.github.com/brysgo/9007731#gistcomment-3696452
    xcode-select --install > /dev/null 2>&1
    if [ 0 == $? ]; then
        sleep 1
        osascript <<EOD
tell application "System Events"
    tell process "Install Command Line Developer Tools"
        keystroke return
        click button "Agree" of window "License Agreement"
    end tell
end tell
EOD
    else
        echo "Command Line Developer Tools are already installed!"
    fi
    # older install
    #in_progress=/tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
    #touch ${in_progress}
    #product=$(softwareupdate --list | awk "/\* Command Line.*${os}/ { sub(/^   \* /, \"\"); print }")
    #softwareupdate --verbose --install "${product}" || echo 'Installation failed.' 1>&2 && rm ${in_progress} && exit 1
    #rm ${in_progress}
    echo 'Installation succeeded.'
fi

sudoers_file="/etc/sudoers.d/${user}"
echo "Checking sudoers.d config ..."
echo "Checking for user: ${user} ..."
echo "Checking for sudo file: ${sudoers_file}"

if [ -f "${sudoers_file}" ]; then
    echo "Sudoers file found: ${sudoers_file}"
else
    echo "Sudoers file not found, creating ..."
    touch ${sudoers_file}
    if [ "$?" -eq 0 ] ; then
        echo "Sudoers file created: ${sudoers_file}"
    else
        echo "Error creating sudoers file, exiting ..."
        exit 1
    fi
fi

sudoers_string="${user} ALL=(ALL:ALL) ALL"

# Check file for content
if [ -s "${sudoers_file}" ] ; then
    echo "File is not empty, continuing without editing"
else
    echo "Populatings sudoers file: ${sudoers_file}"
    echo "${sudoers_string}" >> ${sudoers_file}
    echo "Checking sudoers file: ${sudoers_file}"
    visudo --check --quiet "${sudoers_file}"
    if [ "$?" -ne 0 ] ; then
        echo "Issue in sudoers file, please check manually"
        echo "Command: visudo --check ${sudoers_file}"
        exit 1
    else
        echo "Sudo entry configured for ${user}" 
    fi
fi

echo "Script complete"
echo
exit 0
