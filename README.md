# Machine Setup

Setup your workstation with a single command, or as close as possible.

## Objective

The aim is to setup a workstation, either MacOS or a Linux variant. I don't use
windows and so do not plan to support it currently.

Currently Supported: 
- MacOS

## A Lot To Do

This is a work in progress and should be treated as such.

- [ ] Support Ubuntu/Debian as first linux option
- [ ] init.sh to support Ubuntu/debian

## Setup

This repository assumes you have an administrative account with `root`
permissions via sudo. Like the inital account on a new Mac machine.

Setup requires up to two scripts to be run:

- [init.sh](init.sh) - Installs xcode tools and prepare sudo access for a user
- [setup.sh](setup.sh) - Intalls python and ansible dependencies returning command
  to configure machine

### Pre-requisites

If you already have sudo access or run with an admin level account, then skip
this section and do not run [init.sh](init.sh) script.

Else:

```shell
# Initialise machine with setup tools and give the user "bob" all sudo
sudo MS_USER=bob ./init.sh 
# Or do it interactively
sudo ./init.sh 
```

### Install

Install assumes you are now running as the user you set in `MS_USER`
environmental variable previously.

Run the setup script to install all necessary dependencies:

```shell
./setup.sh
```

Run the command output on terminal to configure you machine based on the
[config.yml](config.yml) file, or defaults when not provided.

```shell
${HOME}/projects/machine-setup/.python/bin/ansible-playbook macos.yml \
-i inventory --ask-become-pass
```

If your run fails around brew or zsh setup, simply re-run. I've had some
intermittent issues but it's aleays worked on second run to completion.

### Customisation

Add your own `config.yml` file to customise any of the available default
config in [default.config.yml](default.config.yml).

Note that some tasks like `gconfigure_projects` may require access to private
git repositories. For this, you will likely need to add you ssh key. Eg.

```shell
ssh-add ~/.ssh/<KEY_NAME>
```

By default ssh based repository urls will use your current key identity for
accessing that repo.

## Acknowledgments

This repo is built off of the awesome work from 
[Jeff Geerling](https://github.com/geerlingguy), in his 
[mac-dev-playbook](https://github.com/geerlingguy/mac-dev-playbook).

Other similar sources of inspiration:

- TBC
